package com.selfstudy.ldapstudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LdapstudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(LdapstudyApplication.class, args);
	}

}
